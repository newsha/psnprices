# README #

This application grabs titlename, releasedate, platforms and all available prices from Sony's Playstation online store.
All data is stored in a database and can be used for creating an overview of PSN's price history. Since the official store doesn't allow the display of past prices, this can help you to keep track of previous sales. Before purchasing a game check its price history. Many of them are back on sale frequently. Easily save money by making well-informed purchasing decisions!

# How To Set Up Project #

1. Clone this project.
2. Set up a database (provided [here](https://bitbucket.org/newsha/psnprices/src/ccbc72c827587d94535c385e000467bb2fc4ab43/PSNPrices/Documentation/Database/?at=master)).
3. Configure src/hibernate.cfg.xml as needed.
4. Paste links of Titles to track in PSNGames.txt, seperated by newline.
5. Run it, wait till finished, check console for progress.

Check [documentation](https://bitbucket.org/newsha/psnprices/src/c01ad75df51af9e3c7888c8d241dfa4298cc6175/PSNPrices/Documentation/?at=master) for more info.

Visit www.psnprices.de for an exemplary presentation of collected prices for over 300 games.