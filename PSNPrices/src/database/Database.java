package database;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import model.Price;
import model.PsnStoreDTO;
import model.Title;

public class Database {

	public static void addTitle(String psnURL) throws MalformedURLException, JSONException, IOException, ParseException {
		

		//check if Title is already in db
		if (isAlreadyTracked(psnURL)) {
			System.out.println("Title already in database with link: " + psnURL);
			return;
		}
		else { 
			
			Session session = HibernateUtil.getSessionFactory().openSession();

			Transaction transaction = session.beginTransaction();
			PsnStoreDTO metaData = new PsnStoreDTO(psnURL);
			
			Title aTitle = new Title(metaData);
			session.save(aTitle);  //updates Title table
			transaction.commit();
			
			updatePrices(psnURL); //updates Prices table
			
			
					}
	}

	/**
	 * checks if Title is already in DB
	 * @param psnURL
	 * @return boolean
	 */
	static boolean isAlreadyTracked(String psnURL) {


		Session session = HibernateUtil.getSessionFactory().openSession();

		String hqlStatement = "Select psnURL from Title";
		Query queryForURLs = session.createQuery(hqlStatement);
		List<?> results = queryForURLs.list();

		for (Object curObject : results) {
			if (curObject.equals(psnURL)) { // if Title already in database
				session.close();
				
				return true;
			}

		}
		session.close();
		
		return false;

	}

	public static void getNewPrices() throws MalformedURLException, JSONException, IOException, ParseException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		String hqlStatement = "Select max(priceDate) from Price";
		
		String latestPriceInDB = (String)session.createQuery(hqlStatement).uniqueResult(); //returns latest priceDate found in db.
		
		System.out.println("im latestPriceInDB String steht: " + latestPriceInDB);
		
		// only update db when no update already done for today
		Calendar currentDate = Calendar.getInstance(); //Get the current date
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
		String dateNow = formatter.format(currentDate.getTime());
		
		if (dateNow.equals(latestPriceInDB)) { // means update has been made today
			System.out.println("Database prices have already been updated today!");
		}
		else {  //no update today yet, so starting price update process...
			hqlStatement = "From Title";
			Query queryForTitles = session.createQuery(hqlStatement);
			@SuppressWarnings("unchecked")
			List<Title> TitleList = queryForTitles.list();

			for (Title curTitle : TitleList)
				updatePrices(curTitle.getPsnURL());
		}
	}
	
	static void updatePrices(String psnURL) throws MalformedURLException, JSONException, IOException, ParseException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction transaction = session.beginTransaction();

		try {
			PsnStoreDTO metaData = new PsnStoreDTO(psnURL);
			
			String hql = "Select id from Title where psnURL = " + "'" + psnURL + "'";  //returns id of specified title
			int idOfTitle = (Integer)session.createQuery(hql).uniqueResult();
			
			session.save(new Price(metaData, idOfTitle));  //stores the regular price of the added Title in db
			
			if(metaData.getPlusPrice()!=0) { //Stores plusPrice, if available
				session.save(new Price(metaData, idOfTitle, true));  //stores the plusPrice of the added Title in db
			}
			
			System.out.println("Title with ID" + idOfTitle + " updated");
			transaction.commit();
		
			
		} 	 catch (Exception FileNotFoundException) {
			// TODO: handle exception
			System.out.println("URL not found: " + psnURL);
		}
			
	}
}