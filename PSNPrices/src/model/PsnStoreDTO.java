package model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;

import org.json.JSONException;
//import org.json.JSONObject;  //need this for url parsing
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Retrieves all relevant information from PSN's json response and saves them.
 * PsnStoreMetaData.class is used for creating objects of Title.class and Price.class. These
 * objects get saved in db.
 * 
 * @author Andi
 *
 */
public class PsnStoreDTO {

	String titleName;
	String releaseDate;
	String psnURL;
	String platform;

	int regularPrice;
	private int plusPrice;

	public PsnStoreDTO(String psnURL) throws MalformedURLException, JSONException, IOException, ParseException {

		String jSonURL = "https://store.playstation.com/store/api/chihiro/00_09_000/container/DE/de/999/";
		jSonURL = jSonURL + psnURL.substring(psnURL.lastIndexOf("/") + 5); // Build
																			// valid
																			// URL
																			// for
																			// returning
																			// the
																			// json
																			// object,
																			// by
																			// getting
																			// die
																			// "cid=..."
																			// part
		
			JSONObject psnJsonData = (JSONObject) new JSONParser()
					.parse(IOUtils.toString(new URL(jSonURL), Charset.forName("UTF-8")));
			this.psnURL = psnURL;
			this.titleName = psnJsonData.get("name").toString(); // this.title =
																	// psnJsonData.get("title_name").toString();
			this.releaseDate = psnJsonData.get("release_date").toString();
			this.releaseDate = this.releaseDate.replace("T00:00:00Z", "");
			this.platform = psnJsonData.get("playable_platform").toString();

			JSONObject default_sku_object = (JSONObject) psnJsonData.get("default_sku"); // Json
																							// object
																							// "default_sku_object",
																							// contains
																							// price
																							// +
																							// special
																							// price
																							// when
																							// avaible

			/* parsing of returned json file */

			this.regularPrice = Integer.parseInt(default_sku_object.get("price").toString()); // get
																								// regular
																								// price
																								// from
																								// default_sku
																								// object

			JSONArray jSonRewardsArray = (JSONArray) default_sku_object.get("rewards"); // jsonArray
																						// with
																						// "rewards"
																						// contents
			// awards has length between 0 and 2. One of them may have
			// "campaign" in them.
			// Iterate jSonRewardsArray and check each index for active
			// campaigns

			for (Object curObject : jSonRewardsArray) { // Check every index of
														// rewardsArray
				JSONObject curJsonObject = (JSONObject) curObject; // Casting
																	// the index
																	// to
																	// JsonObject
																	// to be
																	// able to
																	// access
																	// values
																	// with e.g.
																	// 'jsonObject.get("campaigns")'

				if (Integer.parseInt(curJsonObject.get("discount").toString()) == 100)
					System.out.println("game is free with plus");
				// TODO hier jetzt das attribut is free with plus auf true
				// setzen
				if (curJsonObject.get("campaigns") != null) { // "campaigns"
																// Array exists
																// when
																// plusPrice and
																// regularOffer
																// are avaible,
																// sometimes it
																// also only
																// exists with
																// regular
																// offers only.
																// There doesnt
																// seem to be a
																// logic :(
					if (curJsonObject.get("bonus_price") != null) {
						this.setPlusPrice(Integer.parseInt(curJsonObject.get("bonus_price").toString()));
					}
					this.regularPrice = Integer.parseInt(curJsonObject.get("price").toString());
				}

				else if ((boolean) curJsonObject.get("isPlus")) // When either
																// plus or
																// regular offer
																// alone are
																// available,
																// there is no
																// "campaigns"
																// Array but
																// "end_date"
																// and "isPlus"
																// Strings
					this.setPlusPrice(Integer.parseInt(curJsonObject.get("price").toString()));
				else if (curJsonObject.get("end_date") != null) // must be
																// regular offer
					this.regularPrice = Integer.parseInt(curJsonObject.get("price").toString());

			}

			// System.out.println("MetaDataContents: \n Name: " + title +
			// "\nRelease Date: " + releaseDate + "PSN-URL: " + psnURL +
			// "\nRegular price: " + regularPrice + "\nSale price: " + salePrice
			// + "\nPlus price: " + plusPrice + "\nSystem: " + consoleSystem
			// +"____________________________________________________________________________________________________________________________");

	

	}

	public int getPlusPrice() {
		return plusPrice;
	}

	public void setPlusPrice(int plusPrice) {
		this.plusPrice = plusPrice;
	}

}
