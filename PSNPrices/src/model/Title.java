package model;


public class Title {

	int id;
	String titleName;
	String releaseDate;
	String psnURL;
	String platform; 

	public Title(PsnStoreDTO metaData) {
		
		titleName = metaData.titleName;
		releaseDate = metaData.releaseDate;
		psnURL = metaData.psnURL;
		platform = metaData.platform;
		
		System.out.println("Das Spiel " + titleName +" wurde angelegt");
	//    System.out.println("Data from created Object:\n Name: " + this.title + "\nRelease Date: " + this.releaseDate + "\nPSN-URL: " + psnURL + "\nRegular price: " + this.regularPrice + "\nSale price: " + this.salePrice + "\nPlus price: " + this.plusPrice + "\nSystem: " + this.consoleSystem +"\n _________________________________________________________________");
	

	
	}
	
	Title() {
		
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return titleName;
	}

	public void setTitle(String title) {
		this.titleName = title;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getPsnURL() {
		return psnURL;
	}

	public void setPsnURL(String psnURL) {
		this.psnURL = psnURL;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String consoleSystem) {
		this.platform = consoleSystem;
	}
	
}
