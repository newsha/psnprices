package model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Price {

	int idPrice;
	int price;
	String priceDate;
	boolean isPlus;
	int titleFK;
	
	public Price(PsnStoreDTO metaData, int idOfLastAddedGame) {
		price = metaData.regularPrice;
		priceDate = getTodaysDate();
		isPlus = false;
		titleFK = idOfLastAddedGame;
	}

	public Price(PsnStoreDTO metaData, int idOfLastAddedGame, boolean plusPrice) {

		price = metaData.getPlusPrice();
		priceDate = getTodaysDate();
		isPlus = true;
		titleFK = idOfLastAddedGame;
	}
	Price() {}
	public int getIdPrice() {
		return idPrice;
	}
	public void setIdPrice(int idPrice) {
		this.idPrice = idPrice;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getPriceDate() {
		return priceDate;
	}
	public void setPriceDate(String priceDate) {
		this.priceDate = priceDate;
	}
	public int getTitleFK() {
		return titleFK;
	}
	public void setTitleFK(int titleFK) {
		this.titleFK = titleFK;
	}
	public boolean getisPlus() {
		return isPlus;
	}
	public void setisPlus(boolean isPlus) {
		this.isPlus = isPlus;
	}
	private String getTodaysDate() {
		// TODO Auto-generated method stub
		Calendar currentDate = Calendar.getInstance(); //Get the current date
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
		String dateNow = formatter.format(currentDate.getTime());
		return dateNow;
	}
}
