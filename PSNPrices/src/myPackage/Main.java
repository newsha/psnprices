package myPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.JSONException;
import org.json.simple.parser.ParseException;

import database.Database;
import database.HibernateUtil;

public class Main {

	public static void main(String[] args) throws IOException, JSONException, ParseException {

		Database.getNewPrices(); //update titles already tracked

		File dir = new File(".");
		File txtFile = new File(dir.getCanonicalPath() + File.separator + "PSNGames.txt");

		BufferedReader br = new BufferedReader(new FileReader(txtFile));

		for (String line; (line = br.readLine()) != null;) { //read URLs from .txt and add new titles to db
			Database.addTitle(line);
		}

		br.close();

		HibernateUtil.shutdown();

	}

}
